@extends('template')

@section('title')
Panorama Studio
@stop

@section('content')

<div class="heading"  id="agencia">
    <div class="container  position-relative">
        <div class="row">
            <div class="col-6 no_p">
                <img src="{{ asset('images/head.png')}}" alt="heading">
            </div>
            <div class="col-6">
                <h1 class="animate__animated animate__fadeInRight ">
                    AGENCIA <br>
                    CREATIVA <br>
                    COMUNICACIÓN <br>
                    DIGITAL
                </h1>
            </div>
    
        </div>
        <div class="row">
            <div class="col-6"></div>
            <div class="col-6 no_bg">
                <h2 class="sub_title animate__animated animate__fadeInRight " >
                    Una agencia 360° para contar su historia. <br>
                    Intervenimos en todas las etapas de su proyecto para magnificarlo respetando el hilo conductor y su marca.
                </h2>
            </div>
    
        </div>
        <img src="{{ asset('images/02.png') }}" alt="o" class="position-absolute o animate__animated animate__fadeInBottomLeft">
        
    </div>
</div>

<div class="part_2">
    <div class="filter">
        <div class="container">
            <div class="row">
                <div class="col-6 pad_left">
                    <h2 class="wow bounceInUp animate__animated animate__fadeInRight " >LA AGENCIA</h2>
                    <h3 class="wow bounceInUp animate__animated animate__fadeInRight "  data-wow-delay="1.1s">Con sede en Madrid, hemos querido desarrollar una agencia de un nuevo tipo para proponer un único ecosistema de marketing y comunicación al servicio de sus necesidades.</h3>
                </div>
                <div class="col-6 pad_left">
                    <h2 class="wow bounceInUp animate__animated animate__fadeInRight " >EL EQUIPO</h2>
                    <h3 class="wow bounceInUp animate__animated animate__fadeInRight "  data-wow-delay="1.1s">Compuesto por profesionales de la comunicación, del sector inmobiliario y digital, desde hace 20 años.</h3>
                </div>
            </div>
        </div>
        <div class="separator"></div>
        <div id="estrategia"></div>
    </div>
</div>

<div class="part_3">
    <div class="container" >
        <div class="row" >
            <div class="col-8 position-relative">
                <img src="{{  asset('images/1b.png') }}" alt="one" class="wow bounceInUp animate__animated animate__fadeInRight" >
                <div class="row mt-34">
                    <div class="col-4 wow backInLeft animate__animated animate__backInLeft " >
                        <h3>ANÁLISIS</h3>
                        <p>Las tendencias del mercado, sus objetivos y sus competidores.</p>
                    </div>
                    <div class="col-4 wow backInLeft animate__animated animate__backInLeft " >
                        <h3>formalizacion</h3>
                        <p>Redacción de la plataforma de marca.</p>
                    </div>
                    <div class="col-4 wow backInLeft animate__animated animate__backInLeft " >
                        <h3>orientacion</h3>
                        <p>Gráfica y redacción.</p>
                    </div>
                </div>
                <!--span class="number one">01</span-->

            </div>
            <div class="col-4">
                <h2 class="wow backInRight animate__animated animate__backInRight " >ESTRATEGIA</h2>
                <p class="wow backInRight animate__animated animate__backInRight " data-wow-delay="1.5s">Establecemos una estrategia que será el hilo conductor de todo el proceso. fijar los objetivos. determinar los puntos de distinción, su visión profesional y su posición en el mercado.</p>
            </div>
            <div id="prensa"></div>
        </div>
    
        <div class="row r2" >
            <div class="col-4 contac">
                <h3 class="wow backInRight animate__animated animate__backInRight "  >prensa</h3>
                <p class="wow backInRight animate__animated animate__backInRight "  >Las tendencias del mercado, sus objetivos y sus competidores.</p>
                <div class="social">
                    <a href="#" class="wow animate__animated animate__zoomIn " >
                        <svg xmlns="http://www.w3.org/2000/svg" width="32.511" height="32.51" viewBox="0 0 32.511 32.51">
                            <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M34.411,4.5H7.278A2.652,2.652,0,0,0,4.5,7.032V34.224A2.861,2.861,0,0,0,7.278,37.01H34.4a2.715,2.715,0,0,0,2.608-2.786V7.032A2.493,2.493,0,0,0,34.411,4.5ZM14.577,31.6H9.92V17.118h4.658ZM12.41,14.916h-.034a2.39,2.39,0,0,1-2.456-2.5,2.4,2.4,0,0,1,2.515-2.5,2.4,2.4,0,0,1,2.49,2.5A2.4,2.4,0,0,1,12.41,14.916ZM31.6,31.6H26.941V23.681c0-1.9-.677-3.193-2.363-3.193a2.548,2.548,0,0,0-2.388,1.719,3.136,3.136,0,0,0-.161,1.143V31.6H17.372V17.118H22.03v2.015a4.715,4.715,0,0,1,4.2-2.354c3.057,0,5.369,2.015,5.369,6.36V31.6Z" transform="translate(-4.5 -4.5)" fill="#00224f"/>
                        </svg>
                    </a>
                    <a href="#" class="wow animate__animated animate__zoomIn " >
                        <svg xmlns="http://www.w3.org/2000/svg" width="32.708" height="32.51" viewBox="0 0 32.708 32.51">
                            <path id="Icon_awesome-facebook" data-name="Icon awesome-facebook" d="M33.271,16.917a16.354,16.354,0,1,0-18.91,16.156V21.644H10.207V16.917h4.154v-3.6c0-4.1,2.44-6.362,6.177-6.362A25.169,25.169,0,0,1,24.2,7.27v4.023H22.137a2.364,2.364,0,0,0-2.665,2.554v3.07h4.536l-.725,4.728h-3.81V33.073A16.36,16.36,0,0,0,33.271,16.917Z" transform="translate(-0.563 -0.563)" fill="#00224f"/>
                        </svg>
                    </a>
                    <a href="#" class="wow animate__animated animate__zoomIn ">
                        <svg xmlns="http://www.w3.org/2000/svg" width="36" height="29.239" viewBox="0 0 36 29.239">
                            <path id="Icon_awesome-twitter" data-name="Icon awesome-twitter" d="M32.3,10.668c.023.32.023.64.023.959,0,9.754-7.424,20.992-20.992,20.992A20.85,20.85,0,0,1,0,29.307a15.263,15.263,0,0,0,1.782.091,14.776,14.776,0,0,0,9.16-3.152,7.391,7.391,0,0,1-6.9-5.117,9.3,9.3,0,0,0,1.393.114,7.8,7.8,0,0,0,1.942-.251,7.379,7.379,0,0,1-5.916-7.241V13.66A7.431,7.431,0,0,0,4.8,14.6,7.389,7.389,0,0,1,2.513,4.728a20.972,20.972,0,0,0,15.213,7.721,8.329,8.329,0,0,1-.183-1.69A7.385,7.385,0,0,1,30.312,5.711a14.526,14.526,0,0,0,4.683-1.782,7.358,7.358,0,0,1-3.244,4.066A14.791,14.791,0,0,0,36,6.853a15.86,15.86,0,0,1-3.7,3.815Z" transform="translate(0 -3.381)" fill="#00224f"/>
                        </svg>
                    </a>
                    <a href="#" class="wow animate__animated animate__zoomIn " >
                        <svg xmlns="http://www.w3.org/2000/svg" width="31.518" height="31.51" viewBox="0 0 31.518 31.51">
                            <path id="Icon_awesome-instagram" data-name="Icon awesome-instagram" d="M15.757,9.914a8.079,8.079,0,1,0,8.079,8.079A8.066,8.066,0,0,0,15.757,9.914Zm0,13.331a5.252,5.252,0,1,1,5.252-5.252,5.262,5.262,0,0,1-5.252,5.252ZM26.051,9.584A1.884,1.884,0,1,1,24.166,7.7,1.88,1.88,0,0,1,26.051,9.584ZM31.4,11.5a9.325,9.325,0,0,0-2.545-6.6,9.387,9.387,0,0,0-6.6-2.545c-2.6-.148-10.4-.148-13,0a9.373,9.373,0,0,0-6.6,2.538,9.356,9.356,0,0,0-2.545,6.6c-.148,2.6-.148,10.4,0,13a9.325,9.325,0,0,0,2.545,6.6,9.4,9.4,0,0,0,6.6,2.545c2.6.148,10.4.148,13,0a9.325,9.325,0,0,0,6.6-2.545,9.387,9.387,0,0,0,2.545-6.6c.148-2.6.148-10.392,0-12.994ZM28.041,27.281a5.318,5.318,0,0,1-3,3c-2.074.823-7,.633-9.288.633s-7.221.183-9.288-.633a5.318,5.318,0,0,1-3-3c-.823-2.074-.633-7-.633-9.288s-.183-7.221.633-9.288a5.318,5.318,0,0,1,3-3c2.074-.823,7-.633,9.288-.633s7.221-.183,9.288.633a5.318,5.318,0,0,1,3,3c.823,2.074.633,7,.633,9.288S28.863,25.214,28.041,27.281Z" transform="translate(0.005 -2.238)" fill="#00224f"/>
                        </svg>
                    </a>
                    <a href="#" class="wow animate__animated animate__zoomIn " >
                        <svg xmlns="http://www.w3.org/2000/svg" width="32.51" height="32.51" viewBox="0 0 32.51 32.51">
                            <path id="Icon_awesome-pinterest" data-name="Icon awesome-pinterest" d="M32.51,16.818A16.257,16.257,0,0,1,11.444,32.345a18.272,18.272,0,0,0,2.019-4.26c.2-.76,1.009-3.867,1.009-3.867A4.353,4.353,0,0,0,18.2,26.086c4.9,0,8.436-4.51,8.436-10.114,0-5.368-4.385-9.386-10.022-9.386-7.013,0-10.743,4.706-10.743,9.838,0,2.386,1.272,5.355,3.3,6.3.308.144.472.079.544-.216.052-.223.328-1.331.452-1.842a.487.487,0,0,0-.111-.465,6.4,6.4,0,0,1-1.2-3.71A7.034,7.034,0,0,1,16.19,9.437a6.418,6.418,0,0,1,6.79,6.614c0,4.4-2.222,7.446-5.113,7.446a2.358,2.358,0,0,1-2.406-2.936c.459-1.934,1.344-4.018,1.344-5.414a2.041,2.041,0,0,0-2.058-2.288c-1.632,0-2.943,1.685-2.943,3.946a5.857,5.857,0,0,0,.485,2.412s-1.606,6.8-1.9,8.075a15.735,15.735,0,0,0-.059,4.667A16.257,16.257,0,1,1,32.51,16.818Z" transform="translate(0 -0.563)" fill="#00224f"/>
                        </svg>
                    </a>
                </div>
            </div>
            <div class="col-8 position-relative">
                <!--span class="number two">02</span-->
                <img src="{{  asset('images/2b.png') }}" alt="two" class="wow bounceInUp animate__animated animate__fadeInRight">
            </div>
        </div>
        <div  id="digital"></div>
    </div>
    
</div>
<div class="part_4">
    <div class="container">
        <div class="row">
            <div class="col-8 ">
                <div class="con-7 position-relative">
                    <img src="{{ asset('images/3b.png') }}" alt="4" class="to_t" class="wow bounceInUp animate__animated animate__slideInUp "  >
                    <!--span class="number three">03</span-->    
                </div>
            </div>
            <div class="col-4 t_">
                <h2 class="wow backInRight animate__animated animate__backInRight " >DIGITAL</h2>
                <p>Especialista en imágenes 3D y digital, podemos acompañarle mediante la realización de sitios web dedicados, imágenes y películas 3D únicas.</p>
                <div class="top_ wow animate__animated animate__fadeInBottomRight ">
                    <div class="row">
                        <div class="col-5">
                            <p>VISITA <br>
                                VIRTUAL</p>
                        </div>
                        <div class="col-5">
                            <p>VISITA <br>
                                INMERSIVA</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <p>DECORADOS <br>
                                VIRTUALES</p>
                        </div>
                        <div class="col-5">
                            <p>REALIDAD <br>
                                AUMENTADA</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <p>PELÍCULA <br>
                                ANIMADA</p>
                        </div>
                        <div class="col-5">
                            <p>SALONES Y SHOW <br>
                                ROOM VIRTUALES</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-5">
                            <p>JUEGOS</p>
                        </div>
                        <div class="col-5">
                            <p>SOUND <br>
                                DESIGN</p>
                        </div>
                    </div>
                </div>
                <div id="motion-design"></div>
            </div>

        </div>
        <div class="row tt_" >
            <div class="col-8">
                <div class="con-8 position-relative">

                    <img src="{{ asset('images/5b5.png') }}" alt="5" class="to_t wow animate__animated animate__fadeInUp ">
                    <!--span class="number four">04</span-->    
                </div>
            </div>
            <div class="col-4 t_ no_">
                <h2 class="wow backInRight animate__animated animate__backInRight " >motion design</h2>
                <p class="wow backInRight animate__animated animate__backInRight " >Realizamos show-romos o salones virtuales, que ofrecen un nuevo campo de juego.
                    Creamos decorados para fondos verdes o películas animadas para una comunicación cada vez más afirmada y fortalecer la imagen de las marcas..</p>
            </div>
        </div>
        <div  id="aplicacion"></div>
    </div>
</div>

<div class="part_5 position-relative">
    <div class="container">
        <div class="row" >
            <div class="col-6">
                <h2 class="wow backInRight animate__animated animate__backInRight " >aplicacion</h2>
                <p class="wow backInRight animate__animated animate__backInRight " >Hemos desarrollado varias herramientas interactivas para crear un valor comercial, del que sus clientes no podrán prescindir.</p>
            </div>
            <div class="col-6">
                <img src="{{ asset('images/6.png') }}" alt="6" class="wow bounceInUp animate__animated animate__fadeInRight ">
            </div>
            <div id="espacios" style="height: 30px;"></div>
        </div>

        <div class="row" >
            <div class="col-7">
                <img src="{{ asset('images/7.png') }}" alt="7" class="wow bounceInUp animate__animated animate__fadeInRight ">

            </div>
            <div class="col-5 t_l"> 
                <h2 class="wow backInRight animate__animated animate__backInRight " >espacios <br> efímeros <br> 
                    /pop-up</h2>
                <p class="wow backInRight animate__animated animate__backInRight " >
                    Es una actividad que moviliza el talento creativo y nuestro saber-hacer en la definición y puesta en escena de contenidos. Concedemos la máxima importancia a la creación de atmósferas específicas para que la experiencia del visitante sea diferente.
                </p>
            </div>
        </div>
    </div>
    <img src="{{ asset('images/ooo.png')}}" alt="oo" class="b_o">

</div>
<div class="part_6 position-relative">
    <div class="container ">
        <div class="row">
            <div class="col-4">
                <h3 class="wow animate__animated animate__slideInDown ">CREATIVA</h3>
            </div>
            <div class="col-4">
                <h3 class="wow animate__animated animate__slideInDown ">COMUNICACIÓn</h3>

            </div>
            <div class="col-4">
                <h3 class="wow animate__animated animate__slideInDown ">DIGITAL</h3>
            </div>
        </div>

    </div>
</div>
<div class="part_7">
    <div class="container">
        <div class="row">
            <div class="col-4">
                <h3 class="wow backInRight animate__animated animate__backInLeft " >Contacto</h3>
                <p class="aiz wow backInRight animate__animated animate__backInLeft "  id="c_t">
                    <svg xmlns="http://www.w3.org/2000/svg" width="19.91" height="19.91" viewBox="0 0 19.095 12.89">
                        <path id="Icon_zocial-email" data-name="Icon zocial-email" d="M.072,15.816V5.21q0-.018.055-.35L6.369,10.2.146,16.184a1.561,1.561,0,0,1-.074-.368ZM.9,4.123a.794.794,0,0,1,.313-.055H18.025a1.043,1.043,0,0,1,.331.055L12.1,9.482l-.829.663L9.629,11.489,7.99,10.144l-.829-.663ZM.919,16.9,7.2,10.881l2.431,1.97,2.431-1.97L18.338,16.9a.884.884,0,0,1-.313.055H1.214A.834.834,0,0,1,.919,16.9Zm11.969-6.7,6.224-5.34a1.1,1.1,0,0,1,.055.35V15.816a1.411,1.411,0,0,1-.055.368Z" transform="translate(-0.072 -4.068)" fill="#fff"/>
                    </svg>
                    <a href="mailto:jp@panorama.studio">
                        jp@panorama.studio
                    </a>
                </p>
                <p class="aiz wow backInRight animate__animated animate__backInLeft "  >
                    <svg xmlns="http://www.w3.org/2000/svg" width="19.91" height="19.91" viewBox="0 0 32.511 32.51">
                        <path id="Icon_ionic-logo-linkedin" data-name="Icon ionic-logo-linkedin" d="M34.411,4.5H7.278A2.652,2.652,0,0,0,4.5,7.032V34.224A2.861,2.861,0,0,0,7.278,37.01H34.4a2.715,2.715,0,0,0,2.608-2.786V7.032A2.493,2.493,0,0,0,34.411,4.5ZM14.577,31.6H9.92V17.118h4.658ZM12.41,14.916h-.034a2.39,2.39,0,0,1-2.456-2.5,2.4,2.4,0,0,1,2.515-2.5,2.4,2.4,0,0,1,2.49,2.5A2.4,2.4,0,0,1,12.41,14.916ZM31.6,31.6H26.941V23.681c0-1.9-.677-3.193-2.363-3.193a2.548,2.548,0,0,0-2.388,1.719,3.136,3.136,0,0,0-.161,1.143V31.6H17.372V17.118H22.03v2.015a4.715,4.715,0,0,1,4.2-2.354c3.057,0,5.369,2.015,5.369,6.36V31.6Z" transform="translate(-4.5 -4.5)" fill="#ffffff"/>
                    </svg>
                    <a href="#">
                        panorama-studio-madrid
                    </a>
                </p>
            </div>
            <div class="col-8">
                <form action="{{ route('messages') }}" method="POST" class="form-c">
                    @if(Session::has('success'))
                    <div class="alert alert-success" role="alert">
                        <p>
                            {{ Session::get('success') }}
                        </p>
                    </div>
                    @endif
                    @if(Session::has('errors'))
                    <div class="alert alert-warning" role="warning">
                        <?php
                            $errors = Session::get('errors');
                        ?>
                        
                        <p>
                            {!! implode('', $errors->all('<p>:message</p>')) !!}
                        </p>
                        <?php 
                        ?>
                    </div>
                    @endif
                    @csrf
                    <div class="mb-3">
                        <input type="text" class="form-control" id="nom" placeholder="Nombre" name="nom" required>
                    </div>
                    
                    <div class="mb-3">
                        <input type="email" class="form-control" id="email" placeholder="Email" name="email" required>
                    </div>
                    
                    <div class="mb-3">
                        <input type="text" class="form-control" id="objet" placeholder="Objeto" name="objet" required>
                    </div>
                    <div class="mb-3">
                        <textarea name="message" id="message" cols="30" rows="10" class="form-control" required></textarea>
                    </div>
                    <div class="mb-3">
                        <button type="submit" class="m-btn font-expanded-bold">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22.414" height="22.414" viewBox="0 0 22.414 22.414">
                                <g id="send-svgrepo-com" transform="translate(-1 -0.586)">
                                <line id="Ligne_1" data-name="Ligne 1" x1="11" y2="11" transform="translate(11 2)" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                <path id="Tracé_11757" data-name="Tracé 11757" d="M22,2,15,22l-4-9L2,9Z" fill="none" stroke="#fff" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"/>
                                </g>
                            </svg>
                            <span>Enviar</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <img src="{{ asset('images/ob.png')}}" alt="ob" class="ob">
    </div>
</div>

<a href="" class="back-top-btn">
    <img src="{{ asset('images/up-arrow.png') }}" alt="">
</a>
@stop

@section('scripts')
<script integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous" src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js" integrity="sha512-Eak/29OTpb36LLo2r47IpVzPBLXnAMPAVypbSZiZ4Qkf8p/7S/XRG5xp7OKWPPYfJT6metI+IORkR5G8F900+g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="https://code.jquery.com/jquery-3.6.0.slim.js" integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>
<script src="https://ajax.aspnetcdn.com/ajax/modernizr/modernizr-2.7.2.js"></script>
<script>
    /*
    $(function() {

    var $window           = $(window),
        win_height_padded = $window.height() * 1.1,
        isTouch           = Modernizr.touch;

    if (isTouch) { $('.revealOnScroll').addClass('animated'); }

    $window.on('scroll', revealOnScroll);

    function revealOnScroll() {
        var scrolled = $window.scrollTop(),
            win_height_padded = $window.height() * 1.1;

        // Showed...
        $(".revealOnScroll:not(.animated)").each(function () {
            var $this     = $(this),
                offsetTop = $this.offset().top;

            if (scrolled + win_height_padded > offsetTop) {
            if ($this.data('timeout')) {
                window.setTimeout(function(){
                $this.addClass('animated ' + $this.data('animation'));
                }, parseInt($this.data('timeout'),10));
            } else {
                $this.addClass('animated ' + $this.data('animation'));
            }
            }
        });
        // Hidden...
        $(".revealOnScroll.animated").each(function (index) {
            var $this     = $(this),
                offsetTop = $this.offset().top;
            if (scrolled + win_height_padded < offsetTop) {
            $(this).removeClass('animated fadeInUp flipInX lightSpeedIn')
            }
        });
    }

    revealOnScroll();
   
});
 */
(function() {
    new WOW().init();

   
    document.addEventListener("DOMContentLoaded", function(){
        window.addEventListener('scroll', function() {
        if (window.scrollY > 50) {
            document.getElementById('navbar_top').classList.add('bg-black');
        } else {
            document.getElementById('navbar_top').classList.remove('bg-black');
        } 
    });

    // delays scroll affects
    document.addEventListener("scroll", handleScroll);
    // get a reference to our predefined button
    var scrollToTopBtn = document.querySelector(".back-top-btn");
    scrollToTopBtn.addEventListener("click", scrollToTop);

    function handleScroll() {
        var scrollableHeight = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        var GOLDEN_RATIO = 0.2;

        if ((document.documentElement.scrollTop / scrollableHeight) > GOLDEN_RATIO) {
            //show button
            if (!scrollToTopBtn.classList.contains("showScrollBtn"))
                scrollToTopBtn.classList.add("showScrollBtn")
        } else {
            //hide button
            if (scrollToTopBtn.classList.contains("showScrollBtn"))
                scrollToTopBtn.classList.remove("showScrollBtn")
        }
    }


    function scrollToTop(e) {
        e.preventDefault()
        window.scrollTo({
            top: 0,
            behavior: "smooth"
        });
    }
});
})()
</script>
@stop