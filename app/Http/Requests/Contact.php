<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Contact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => ['required', 'string', 'alpha_num', 'max:255'],
            //'prenom' => 'required|string|alpha_num|max:255',
            'email' => ['required', 'string', 'email', 'max:255',],
            'objet' => ['required', 'string', 'alpha_num', 'max:255'],
            'message' => ['required', 'string', 'alpha_num', 'max:500'],
        ];
    }
}