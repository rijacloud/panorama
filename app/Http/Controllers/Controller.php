<?php

namespace App\Http\Controllers;

use App\Http\Requests\Contact;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function message(Contact $request)
    {

        $nom = $request->get('nom');
        //$prenom = $request->get('prenom');
        $email = $request->get('email');
        $objet = $request->get('objet');
        $mesg = $request->get('message');

        $to_name = 'No reply - panorama.studio';
        $to_email = 'jp@panorama.studio';
        //$to_email = 'rijatarashi@gmail.com';

        Mail::send('emails.message', [
            "nom" => strip_tags($nom),
            //"prenom" => strip_tags($prenom),
            "email" => strip_tags($email),
            "objet" => strip_tags($objet),
            "mesg" => strip_tags($mesg)
        ], function ($message) use ($to_name, $to_email, $objet) {
            $message->to($to_email, $to_name)
                ->subject('Message - ' . $objet);
            $message->from('no-reply@panoramma.studio', 'Message');
        });

        return redirect()->route('home')->with('success', 'Tu mensaje ha sido enviado');
    }
}